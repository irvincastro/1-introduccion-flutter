//CLASE 4.4 Switch y case
void main(){
  var estadoTienda = 'ABIERTA';
  
  switch(estadoTienda){
    case 'ABIERTA':
      print('Tienda abierta');
      break;
    case 'CERRADA':
      print('Tienda cerrada');
      break;
  }
}