// Función main
main() {
  int a = 1;
  double b = 2.2;
  
  //int
  var x = 1;
  //double
  var y = 1.1;
   
  //num es de ambos tipos: int y double
  num z = -1;
  
  //Método abs() obtiene el valor absoluto
  print(z.abs());
  //Método ceil() redondea hacia arriba
  print(y.ceil()); //Redondea al siguiente número entero
  //Método floor() redondea hacia abajo
  print(y.floor()); //Redondea al número entero actual
  
  double real = 1; //Dart convierte el valor a 1.0
  print(real);
  
  //Convertir String --> int
  int uno = int.parse('1');
  
  //Convertir String --> double
  double unoPuntoUno = double.parse('1.1');
  
  //Convertir int --> String
  String cadenaUno = uno.toString();
  
  //Lo mismo con un double
  
  double pi = 3.1416;
  String cadenaPi = pi.toStringAsFixed(2); //Método convierte y poder indicar cuantos decimales.
  print(cadenaPi);
}