//CLASE 3.1 Funciones
// Función main
main() {
  
  //Declaración de método
  bool esPar(int numero){
    return numero % 2 == 0;
  }
  
  print('El n° 2 es par? ${esPar(2)}');
  print('El n° 3 es par? ${esPar(3)}');
  
  //Declaración de método con una sola sentencia
  esImpar(int numero) => numero % 2 != 0;
  
  print('El n° 3 es impar? ${esImpar(3)}');
  print('El n° 2 es impar? ${esImpar(2)}');
  
  //Método o función que imprime un texto
  imprime() => print('Prueba');
  imprime();
  
  //Método o función que devuelve el resultado de una suma
  int x = 1;
  int y = 3;
  suma(int a, b) => a + b;
  print('La suma es: ${suma(x,y)}');
}